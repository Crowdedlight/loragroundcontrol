//
// Created by crow on 26/11/18.
//

#ifndef LORA_GROUND_CONTROL_DRONEITEM_H
#define LORA_GROUND_CONTROL_DRONEITEM_H

#include <QColor>
#include <QGeoCoordinate>
#include <QString>

class DroneItem {
public:
    QString name() const;
    void setName(const QString &name);

    QGeoCoordinate getDronePos() const;
    void setDronePos(const QGeoCoordinate &pos);

    void appendHistory(const QGeoCoordinate &value);
    QList<QGeoCoordinate> getHistory() const;

    QColor getColor() const;
    void setColor(const QColor &color);

    int getAngle() const;
    void setAngle(int angle);

private:
    QString _name;
    QGeoCoordinate _dronePos;
    QList<QGeoCoordinate> _history;
    QColor _color;
    int _angle;
};


#endif //LORA_GROUND_CONTROL_DRONEITEM_H
