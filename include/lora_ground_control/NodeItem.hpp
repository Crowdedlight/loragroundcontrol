//
// Created by Frederik Mazur Andersen on 26/11/18.
//

#ifndef LORA_GROUND_CONTROL_NODEITEM_HPP
#define LORA_GROUND_CONTROL_NODEITEM_HPP

#include <QString>
#include <ros/ros.h>
#include <QtCore/QMetaType>
#include <QObject>

class NodeItem {


public:
    NodeItem();

    QString name() const;
    unsigned int id() const;
    float expected_interval() const;
    ros::Time lastReceived() const;
    bool hasError() const;
    QString main_status() const;

    QString current_task() const;
    NodeItem(unsigned int id, QString name, float expected_interval, QString main_status, QString current_task);

    void updateItem(QString main_status, QString current_task, bool hasError);
    void timeoutError();

    virtual ~NodeItem();

private:
    unsigned int _id;
    QString _name;
    float _expected_interval;
    QString _main_status;
    QString _current_task;
    ros::Time _lastReceived;

    bool _hasError = false;
};

Q_DECLARE_METATYPE(NodeItem)

#endif //LORA_GROUND_CONTROL_NODEITEM_HPP
