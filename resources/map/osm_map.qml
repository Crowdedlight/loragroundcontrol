import QtQuick 2.4
import QtPositioning 5.9
import QtLocation 5.9
import QtQuick.Controls 1.4

Rectangle {
    anchors.fill: parent
    id: map_rect

    Plugin {
        id: here_plugin
        // name: "osm"
        // PluginParameter { name: "osm.useragent"; value: "LoraGroundControl" }
        name: "here"
        PluginParameter { name: "here.app_id"; value: "H98M1rtC80OwOGoS8LzJ" }
        PluginParameter { name: "here.token"; value: "f2NIL2P0O--fOtFbPk_Ciw" }
    }

    Map {
        id: map
        anchors.centerIn: parent
        anchors.fill: parent
        plugin: here_plugin
        zoomLevel: 10
        center: mapController.centerViewPos
        gesture.acceptedGestures:   MapGestureArea.PinchGesture | MapGestureArea.PanGesture | MapGestureArea.FlickGesture
        gesture.flickDeceleration:  3000

        MapItemView {
            model: droneModel
            delegate: DroneMarker {
                coordinate: positionData
                name: nameData
                transformOrigin: Item.center
                rotation: angleData
                z: 100
            }
        }

        MapItemView {
            model: droneModel
            delegate: DroneTrail {
                path: historyData
                line.color: colorData
            }
        }
    }   

    Item{
       id:controlPanel
       width:controls.childrenRect.width
       height:controls.childrenRect.height
       Rectangle{
           anchors.fill: parent
           color:"lightgreen"
           opacity:0.6
           radius:12
       }
       Flow{
            id:controls
            width:parent.width

            GroupBox{
                title: "Map Types"
                // Text {
                //     id: map_type_label
                //     text: "Map Types" 
                //     anchors.fill: parent
                //     anchors.bottom: map_type_combobox.top
                //     horizontalAlignment: Text.AlignHCenter
                // }
                ComboBox{
                    id: map_type_combobox
                    model:map.supportedMapTypes
                    textRole:"description"   
                    currentIndex: 3
                    Component.onCompleted: map.activeMapType = map.supportedMapTypes[3]
                    onCurrentIndexChanged: map.activeMapType = map.supportedMapTypes[currentIndex]
               }
            }
       }
   } 
}
